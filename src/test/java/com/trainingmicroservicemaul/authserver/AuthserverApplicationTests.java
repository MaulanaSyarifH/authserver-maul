package com.trainingmicroservicemaul.authserver;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthserverApplicationTests {

	@Autowired private PasswordEncoder passwordEncoder;

	@Test
	public void testGeneratePasswordBcrypt() {
		String passwordStaff = "teststaff";
		String bcryptStaff = passwordEncoder.encode(passwordStaff);
		Assert.assertNotNull(bcryptStaff);
		System.out.println("teststaff : "+bcryptStaff);

		String passwordManager = "testmanager";
		String bcryptManager = passwordEncoder.encode(passwordManager);
		Assert.assertNotNull(bcryptManager);
		System.out.println("testmanager : "+bcryptManager);
		
		String passwordMol = "mol123";
		String bcryptMol = passwordEncoder.encode(passwordMol);
		Assert.assertNotNull(bcryptMol);
		System.out.println("mol123 : "+bcryptMol);
	}



}
